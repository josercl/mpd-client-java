package com.gitlab.josercl.mpd;

import com.gitlab.josercl.mpd.enums.MPDCommand;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommandTest {
    @Test
    void TestToString() {
        Command testCommand = new Command(MPDCommand.PLAY);

        assertEquals("play", testCommand.toString());
    }

    @Test
    void TestToStringWithEmptyArgs() {
        Command testCommand = new Command(MPDCommand.PLAY, " ");

        assertEquals("play", testCommand.toString());
    }

    @Test
    void TestToStringWithNonEmptyArgs() {
        Command testCommand = new Command(MPDCommand.PLAY, "123", " ", "456");

        assertEquals("play 123 456", testCommand.toString());
    }
}