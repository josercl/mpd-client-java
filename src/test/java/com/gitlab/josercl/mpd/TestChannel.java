package com.gitlab.josercl.mpd;

import com.gitlab.josercl.mpd.exception.InvalidResponseException;

import java.util.List;

public class TestChannel implements IChannel{
    @Override
    public String getHost() {
        return "localhost";
    }

    @Override
    public int getPort() {
        return 1234;
    }

    @Override
    public void executeCommands(Command... commands) throws InvalidResponseException {

    }

    @Override
    public List<String> executeCommand(Command command) throws InvalidResponseException {
        return null;
    }
}
