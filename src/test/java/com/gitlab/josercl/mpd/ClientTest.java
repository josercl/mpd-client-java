package com.gitlab.josercl.mpd;

import com.gitlab.josercl.mpd.parser.OutputParser;
import com.gitlab.josercl.mpd.parser.SongParser;
import com.gitlab.josercl.mpd.parser.StatusParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClientTest {
    private IChannel channel;

    private Client client;

    @BeforeEach
    void setUp() {
        channel = new TestChannel();
        client = new Client(channel, new StatusParser(), new SongParser(), new OutputParser());
    }

    @Test
    void testGetHost() {
        assertEquals(channel.getHost(), client.getHost());
        assertEquals(channel.getPort(), client.getPort());
    }
}