package com.gitlab.josercl.mpd.parser;

import com.gitlab.josercl.mpd.enums.Single;
import com.gitlab.josercl.mpd.enums.State;
import com.gitlab.josercl.mpd.exception.InvalidFormatException;
import com.gitlab.josercl.mpd.model.Status;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class StatusParserTest {
    private StatusParser parser;

    String[] lines = new String[]{
            "volume: 56",
            "repeat: 1",
            "random: 1",
            "single: 0",
            "consume: 0",
            "partition: partitionX",
            "playlist: 100",
            "playlistlength: 99",
            "mixrampdb: 0",
            "state: pause",
            "song: 47",
            "songid: 48",
            "time: 280:331",
            "elapsed: 279.889",
            "bitrate: 320",
            "duration: 330.866",
            "audio: 44100:24:2",
            "nextsong: 30",
            "nextsongid: 31",
            "xfade: 45",
            "mixrampdelay: 56",
            "updating_db: 567"
    };

    @BeforeEach
    void setUp() {
        parser = new StatusParser();
    }

    @Test
    void parseStatus() {
        Status parsedStatus = parser.parse(Arrays.asList(lines));
        assertEquals(56, parsedStatus.getVolume());
        assertTrue(parsedStatus.isRepeat());
        assertTrue(parsedStatus.isRandom());
        assertEquals(Single.NO, parsedStatus.getSingle());
        assertFalse(parsedStatus.isConsume());
        assertEquals("partitionX", parsedStatus.getPartition());
        assertEquals(100, parsedStatus.getPlaylist());
        assertEquals(99, parsedStatus.getPlaylistLength());
        assertEquals(0, parsedStatus.getMixrampdb());
        assertEquals(State.PAUSED, parsedStatus.getState());
        assertEquals(47, parsedStatus.getSong());
        assertEquals(48, parsedStatus.getSongId());
        assertEquals(279.889, parsedStatus.getElapsed());
        assertEquals(330.866, parsedStatus.getDuration());
        assertEquals(320, parsedStatus.getBitrate());
        assertEquals(30, parsedStatus.getNextSong());
        assertEquals(31, parsedStatus.getNextSongId());
        assertEquals("44100:24:2", parsedStatus.getAudio());
        assertEquals(45, parsedStatus.getCrossfade());
        assertEquals(56, parsedStatus.getMixrampdelay());
        assertEquals(567, parsedStatus.getUpdatingDb());
    }

    @Test
    void parseStatusError() {
        Status parsedStatus = parser.parse(Collections.singletonList("error: This is an error"));
        assertEquals("This is an error", parsedStatus.getError());
    }

    @Test
    void testInvalidResponse() {
        assertThrows(
                InvalidFormatException.class,
                () -> parser.parse(Collections.singletonList("should be an exception"))
        );
    }

    @Test
    void testEquals() {
        Status parsedStatus = parser.parse(Arrays.asList(lines));
        Status createdStatus = new Status.Builder()
                .setVolume(56).setRepeat(true).setRandom(true).setSingle(Single.NO).setConsume(false)
                .setPartition("partitionX").setPlaylist(100L).setPlaylistLength(99).setMixrampdb(0d)
                .setState(State.PAUSED).setSong(47).setSongId(48L).setElapsed(279.889).setBitrate(320)
                .setDuration(330.866).setAudio("44100:24:2").setNextSong(30).setNextSongId(31L).setCrossfade(45d)
                .setMixrampdelay(56d).setUpdatingDb(567L)
                .build();

        assertEquals(parsedStatus.hashCode(), createdStatus.hashCode());
        assertEquals(parsedStatus, createdStatus);
    }

    @Test
    void testToString() {
        Status parsedStatus = parser.parse(Arrays.asList(lines));
        String expected = "Status{" +
                "partition='partitionX'" +
                ", volume=56" +
                ", repeat=1" +
                ", random=1" +
                ", single=0" +
                ", consume=0" +
                ", playlist=100" +
                ", playlistLength=99" +
                ", state=pause" +
                ", song=47" +
                ", songId=48" +
                ", nextSong=30" +
                ", nextSongId=31" +
                ", elapsed=279.889" +
                ", duration=330.866" +
                ", bitrate=320" +
                ", crossfade=45" +
                ", mixrampdb=0" +
                ", mixrampdelay=56" +
                ", audio=44100:24:2" +
                ", updatingDb=567" +
                ", error=''" +
                '}';
        assertEquals(expected, parsedStatus.toString());
    }
}