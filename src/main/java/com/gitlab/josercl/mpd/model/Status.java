package com.gitlab.josercl.mpd.model;

import com.gitlab.josercl.mpd.enums.Single;
import com.gitlab.josercl.mpd.enums.State;

import java.util.Objects;

public class Status {
    private final String partition;
    private final Integer volume;
    private final boolean repeat;
    private final boolean random;
    private final Single single;
    private final boolean consume;
    private final Long playlist;
    private final Integer playlistLength;
    private final State state;
    private final Integer song;
    private final Long songId;
    private final Integer nextSong;
    private final Long nextSongId;
    private final Double elapsed;
    private final Double duration;
    private final Integer bitrate;
    private final Double crossfade;
    private final Double mixrampdb;
    private final Double mixrampdelay;
    private final String audio;
    private final Long updatingDb;
    private final String error;

    private Status(
            String partition,
            Integer volume,
            boolean repeat,
            boolean random,
            Single single,
            boolean consume,
            Long playlist,
            Integer playlistLength,
            State state,
            Integer song,
            Long songId,
            Integer nextSong,
            Long nextSongId,
            Double elapsed,
            Double duration,
            Integer bitrate,
            Double crossfade,
            Double mixrampdb,
            Double mixrampdelay,
            String audio,
            Long updatingDb,
            String error) {
        this.partition = partition;
        this.volume = volume;
        this.repeat = repeat;
        this.random = random;
        this.single = single;
        this.consume = consume;
        this.playlist = playlist;
        this.playlistLength = playlistLength;
        this.state = state;
        this.song = song;
        this.songId = songId;
        this.nextSong = nextSong;
        this.nextSongId = nextSongId;
        this.elapsed = elapsed;
        this.duration = duration;
        this.bitrate = bitrate;
        this.crossfade = crossfade;
        this.mixrampdb = mixrampdb;
        this.mixrampdelay = mixrampdelay;
        this.audio = audio;
        this.updatingDb = updatingDb;
        this.error = error;
    }

    public String getPartition() {
        return partition;
    }

    public Integer getVolume() {
        return volume;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public boolean isRandom() {
        return random;
    }

    public Single getSingle() {
        return single;
    }

    public boolean isConsume() {
        return consume;
    }

    public Long getPlaylist() {
        return playlist;
    }

    public Integer getPlaylistLength() {
        return playlistLength;
    }

    public State getState() {
        return state;
    }

    public Integer getSong() {
        return song;
    }

    public Long getSongId() {
        return songId;
    }

    public Integer getNextSong() {
        return nextSong;
    }

    public Long getNextSongId() {
        return nextSongId;
    }

    public Double getElapsed() {
        return elapsed;
    }

    public Double getDuration() {
        return duration;
    }

    public Integer getBitrate() {
        return bitrate;
    }

    public Double getCrossfade() {
        return crossfade;
    }

    public Double getMixrampdb() {
        return mixrampdb;
    }

    public Double getMixrampdelay() {
        return mixrampdelay;
    }

    public String getAudio() {
        return audio;
    }

    public Long getUpdatingDb() {
        return updatingDb;
    }

    public String getError() {
        return error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Status status = (Status) o;

        if (repeat != status.repeat) return false;
        if (random != status.random) return false;
        if (consume != status.consume) return false;
        if (!Objects.equals(partition, status.partition)) return false;
        if (!Objects.equals(volume, status.volume)) return false;
        if (single != status.single) return false;
        if (!Objects.equals(playlist, status.playlist)) return false;
        if (!Objects.equals(playlistLength, status.playlistLength)) return false;
        if (state != status.state) return false;
        if (!Objects.equals(song, status.song)) return false;
        if (!Objects.equals(songId, status.songId)) return false;
        if (!Objects.equals(nextSong, status.nextSong)) return false;
        if (!Objects.equals(nextSongId, status.nextSongId)) return false;
        if (!Objects.equals(elapsed, status.elapsed)) return false;
        if (!Objects.equals(duration, status.duration)) return false;
        if (!Objects.equals(bitrate, status.bitrate)) return false;
        if (!Objects.equals(crossfade, status.crossfade)) return false;
        if (!Objects.equals(mixrampdb, status.mixrampdb)) return false;
        if (!Objects.equals(mixrampdelay, status.mixrampdelay)) return false;
        if (!Objects.equals(audio, status.audio)) return false;
        if (!Objects.equals(updatingDb, status.updatingDb)) return false;
        return Objects.equals(error, status.error);
    }

    @Override
    public int hashCode() {
        int result = partition != null ? partition.hashCode() : 0;
        result = 31 * result + (volume != null ? volume.hashCode() : 0);
        result = 31 * result + (repeat ? 1 : 0);
        result = 31 * result + (random ? 1 : 0);
        result = 31 * result + (single != null ? single.hashCode() : 0);
        result = 31 * result + (consume ? 1 : 0);
        result = 31 * result + (playlist != null ? playlist.hashCode() : 0);
        result = 31 * result + (playlistLength != null ? playlistLength.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (song != null ? song.hashCode() : 0);
        result = 31 * result + (songId != null ? songId.hashCode() : 0);
        result = 31 * result + (nextSong != null ? nextSong.hashCode() : 0);
        result = 31 * result + (nextSongId != null ? nextSongId.hashCode() : 0);
        result = 31 * result + (elapsed != null ? elapsed.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (bitrate != null ? bitrate.hashCode() : 0);
        result = 31 * result + (crossfade != null ? crossfade.hashCode() : 0);
        result = 31 * result + (mixrampdb != null ? mixrampdb.hashCode() : 0);
        result = 31 * result + (mixrampdelay != null ? mixrampdelay.hashCode() : 0);
        result = 31 * result + (audio != null ? audio.hashCode() : 0);
        result = 31 * result + (updatingDb != null ? updatingDb.hashCode() : 0);
        result = 31 * result + (error != null ? error.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Status{" +
                "partition='" + partition + '\'' +
                ", volume=" + volume +
                ", repeat=" + repeat +
                ", random=" + random +
                ", single=" + single +
                ", consume=" + consume +
                ", playlist=" + playlist +
                ", playlistLength=" + playlistLength +
                ", state=" + state +
                ", song=" + song +
                ", songId=" + songId +
                ", nextSong=" + nextSong +
                ", nextSongId=" + nextSongId +
                ", elapsed=" + elapsed +
                ", duration=" + duration +
                ", bitrate=" + bitrate +
                ", crossfade=" + crossfade +
                ", mixrampdb=" + mixrampdb +
                ", mixrampdelay=" + mixrampdelay +
                ", audio='" + audio + '\'' +
                ", updatingDb=" + updatingDb +
                ", error='" + error + '\'' +
                '}';
    }

    public static class Builder {
        private String partition;
        private Integer volume = 0;
        private boolean repeat;
        private boolean random;
        private Single single;
        private boolean consume;
        private Long playlist;
        private Integer playlistLength;
        private State state;
        private Integer song;
        private Long songId;
        private Integer nextSong;
        private Long nextSongId;
        private Double elapsed = 0d;
        private Double duration = 0d;
        private Integer bitrate;
        private Double crossfade;
        private Double mixrampdb;
        private Double mixrampdelay;
        private String audio;
        private Long updatingDb;
        private String error;

        public Builder setPartition(String partition) {
            this.partition = partition;
            return this;
        }

        public Builder setVolume(Integer volume) {
            this.volume = volume;
            return this;
        }

        public Builder setRepeat(boolean repeat) {
            this.repeat = repeat;
            return this;
        }

        public Builder setRandom(boolean random) {
            this.random = random;
            return this;
        }

        public Builder setSingle(Single single) {
            this.single = single;
            return this;
        }

        public Builder setConsume(boolean consume) {
            this.consume = consume;
            return this;
        }

        public Builder setPlaylist(Long playlist) {
            this.playlist = playlist;
            return this;
        }

        public Builder setPlaylistLength(Integer playlistLength) {
            this.playlistLength = playlistLength;
            return this;
        }

        public Builder setState(State state) {
            this.state = state;
            return this;
        }

        public Builder setSong(Integer song) {
            this.song = song;
            return this;
        }

        public Builder setSongId(Long songId) {
            this.songId = songId;
            return this;
        }

        public Builder setNextSong(Integer nextSong) {
            this.nextSong = nextSong;
            return this;
        }

        public Builder setNextSongId(Long nextSongId) {
            this.nextSongId = nextSongId;
            return this;
        }

        public Builder setElapsed(Double elapsed) {
            this.elapsed = elapsed;
            return this;
        }

        public Builder setDuration(Double duration) {
            this.duration = duration;
            return this;
        }

        public Builder setBitrate(Integer bitrate) {
            this.bitrate = bitrate;
            return this;
        }

        public Builder setCrossfade(Double crossfade) {
            this.crossfade = crossfade;
            return this;
        }

        public Builder setMixrampdb(Double mixrampdb) {
            this.mixrampdb = mixrampdb;
            return this;
        }

        public Builder setMixrampdelay(Double mixrampdelay) {
            this.mixrampdelay = mixrampdelay;
            return this;
        }

        public Builder setAudio(String audio) {
            this.audio = audio;
            return this;
        }

        public Builder setUpdatingDb(Long updatingDb) {
            this.updatingDb = updatingDb;
            return this;
        }

        public Builder setError(String error) {
            this.error = error;
            return this;
        }

        public Status build() {
            return new Status(
                    partition,
                    volume,
                    repeat,
                    random,
                    single,
                    consume,
                    playlist,
                    playlistLength,
                    state,
                    song,
                    songId,
                    nextSong,
                    nextSongId,
                    elapsed,
                    duration,
                    bitrate,
                    crossfade,
                    mixrampdb,
                    mixrampdelay,
                    audio,
                    updatingDb,
                    error);
        }
    }
}
