package com.gitlab.josercl.mpd.model;

import java.util.Date;
import java.util.Objects;

public class Song {
    private final String file;
    private final Date lastModified;
    private final String artist;
    private final String albumArtist;
    private final String title;
    private final String album;
    private final int track;
    private final String date;
    private final String genre;
    private final String conductor;
    private final String grouping;
    private final String label;
    private final int duration;
    private final int pos;
    private final Long id;

    public Song(
            String file,
            Date lastModified,
            String artist,
            String albumArtist,
            String title,
            String album,
            int track,
            String date,
            String genre,
            String conductor,
            String grouping,
            String label,
            int duration,
            int pos,
            Long id
    ) {
        this.file = file;
        this.lastModified = lastModified;
        this.artist = artist;
        this.albumArtist = albumArtist;
        this.title = title;
        this.album = album;
        this.track = track;
        this.date = date;
        this.genre = genre;
        this.conductor = conductor;
        this.grouping = grouping;
        this.label = label;
        this.duration = duration;
        this.pos = pos;
        this.id = id;
    }

    public String getFile() {
        return file;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public String getArtist() {
        return artist;
    }

    public String getAlbumArtist() {
        return albumArtist;
    }

    public String getTitle() {
        return title;
    }

    public String getAlbum() {
        return album;
    }

    public int getTrack() {
        return track;
    }

    public String getDate() {
        return date;
    }

    public String getGenre() {
        return genre;
    }

    public String getConductor() {
        return conductor;
    }

    public String getGrouping() {
        return grouping;
    }

    public String getLabel() {
        return label;
    }

    public int getDuration() {
        return duration;
    }

    public int getPos() {
        return pos;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Song{" +
                "file='" + file + '\'' +
                ", lastModified=" + lastModified +
                ", artist='" + artist + '\'' +
                ", albumArtist='" + albumArtist + '\'' +
                ", title='" + title + '\'' +
                ", album='" + album + '\'' +
                ", track=" + track +
                ", date='" + date + '\'' +
                ", genre='" + genre + '\'' +
                ", conductor='" + conductor + '\'' +
                ", grouping='" + grouping + '\'' +
                ", label='" + label + '\'' +
                ", duration=" + duration +
                ", pos=" + pos +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Song song = (Song) o;

        if (track != song.track) return false;
        if (duration != song.duration) return false;
        if (pos != song.pos) return false;
        if (!Objects.equals(file, song.file)) return false;
        if (!Objects.equals(lastModified, song.lastModified)) return false;
        if (!Objects.equals(artist, song.artist)) return false;
        if (!Objects.equals(albumArtist, song.albumArtist)) return false;
        if (!Objects.equals(title, song.title)) return false;
        if (!Objects.equals(album, song.album)) return false;
        if (!Objects.equals(date, song.date)) return false;
        if (!Objects.equals(genre, song.genre)) return false;
        if (!Objects.equals(conductor, song.conductor)) return false;
        if (!Objects.equals(grouping, song.grouping)) return false;
        if (!Objects.equals(label, song.label)) return false;
        return Objects.equals(id, song.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(file, lastModified, artist, albumArtist, title, album, track, date, genre, conductor, grouping, label, duration, pos, id);
    }

    public static class Builder {
        private String file = "";
        private Date lastModified;
        private String artist = "";
        private String albumArtist = "";
        private String title = "";
        private String album = "";
        private int track;
        private String date;
        private String genre;
        private String conductor;
        private String grouping;
        private String label;
        private int duration = 0;
        private int pos = 0;
        private Long id = 0L;

        public Builder setFile(String file) {
            this.file = file;
            return this;
        }

        public Builder setLastModified(Date lastModified) {
            this.lastModified = lastModified;
            return this;
        }

        public Builder setArtist(String artist) {
            this.artist = artist;
            return this;
        }

        public Builder setAlbumArtist(String albumArtist) {
            this.albumArtist = albumArtist;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setAlbum(String album) {
            this.album = album;
            return this;
        }

        public Builder setTrack(int track) {
            this.track = track;
            return this;
        }

        public Builder setDate(String date) {
            this.date = date;
            return this;
        }

        public Builder setGenre(String genre) {
            this.genre = genre;
            return this;
        }

        public Builder setConductor(String conductor) {
            this.conductor = conductor;
            return this;
        }

        public Builder setGrouping(String grouping) {
            this.grouping = grouping;
            return this;
        }

        public Builder setLabel(String label) {
            this.label = label;
            return this;
        }

        public Builder setDuration(int duration) {
            this.duration = duration;
            return this;
        }

        public Builder setPos(int pos) {
            this.pos = pos;
            return this;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Song build() {
            return new Song(
                    file,
                    lastModified,
                    artist,
                    albumArtist,
                    title,
                    album,
                    track,
                    date,
                    genre,
                    conductor,
                    grouping,
                    label,
                    duration,
                    pos,
                    id
            );
        }
    }
}
