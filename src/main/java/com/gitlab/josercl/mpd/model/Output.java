package com.gitlab.josercl.mpd.model;

/**
 * MPD Outputs
 */
public class Output {
    /**
     * Output ID
     */
    private final Long id;
    /**
     * Output name
     */
    private final String name;
    /**
     * Output plugin
     */
    private final String plugin;
    /**
     * Output enabled state
     */
    private final boolean enabled;

    private Output(Long id, String name, String plugin, boolean enabled) {
        this.id = id;
        this.name = name;
        this.plugin = plugin;
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPlugin() {
        return plugin;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public static class Builder {
        private Long id;
        private String name;
        private String plugin;
        private boolean enabled;

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setPlugin(String plugin) {
            this.plugin = plugin;
            return this;
        }

        public Builder setEnabled(boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public Output build() {
            return new Output(id, name, plugin, enabled);
        }
    }

    @Override
    public String toString() {
        return "Output{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", plugin='" + plugin + '\'' +
                ", enabled=" + enabled +
                '}';
    }
}
