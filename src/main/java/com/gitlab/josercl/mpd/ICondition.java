package com.gitlab.josercl.mpd;

public interface ICondition {
    String toString();
    String[] toStringArray();
}
