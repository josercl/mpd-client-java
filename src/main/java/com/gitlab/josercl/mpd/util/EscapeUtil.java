package com.gitlab.josercl.mpd.util;

import java.util.Optional;

public final class EscapeUtil {
    public static String escape(String text) {
        return Optional.ofNullable(text)
                .map(s -> text.replaceAll("\"", "\\\\\"")
                        .replaceAll("'", "\\\\'"))
                .orElse("");
    }

    public static String quote(String text) {
        return Optional.ofNullable(text)
                .map(s -> String.format("\"%s\"", escape(s)))
                .orElse("");
    }
}
