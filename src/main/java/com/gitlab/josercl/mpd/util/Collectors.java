package com.gitlab.josercl.mpd.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class Collectors {
    public static <T> Collector<T, ArrayList<T>, T[]> toArray(Class<T> clazz) {
        return new Collector<T, ArrayList<T>, T[]>() {
            @Override
            public Supplier<ArrayList<T>> supplier() {
                return ArrayList::new;
            }

            @Override
            public BiConsumer<ArrayList<T>, T> accumulator() {
                return ArrayList::add;
            }

            @Override
            public BinaryOperator<ArrayList<T>> combiner() {
                return (ts, ts2) -> {
                    ts.addAll(ts2);
                    return ts;
                };
            }

            @Override
            public Function<ArrayList<T>, T[]> finisher() {
                return ts -> {
                    T[] arr = (T[]) Array.newInstance(clazz, ts.size());
                    ts.toArray(arr);
                    return arr;
                };
            }

            @Override
            public Set<Characteristics> characteristics() {
                return new HashSet<>();
            }
        };
    }
}
