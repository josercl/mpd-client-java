package com.gitlab.josercl.mpd.enums;

import java.util.Arrays;

public enum Single {
    NO("0"),
    YES("1"),
    ONESHOT("oneshot"),
    INVALID("invalid");

    private final String value;

    Single(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Single fromValue(String val) {
        return Arrays.stream(values())
                .filter(s -> s.getValue().equals(val))
                .findFirst()
                .orElse(INVALID);
    }

    @Override
    public String toString() {
        return getValue();
    }
}
