package com.gitlab.josercl.mpd.enums;

import java.util.Arrays;

public enum State {
    PLAYING("play"),
    PAUSED("pause"),
    STOPPED("stop"),
    INVALID("invalid");

    private final String value;

    State(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static State fromValue(String val) {
        return Arrays.stream(values())
                .filter(s -> s.getValue().equals(val))
                .findFirst()
                .orElse(INVALID);
    }
}
