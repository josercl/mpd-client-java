package com.gitlab.josercl.mpd.enums;

public enum Tag {
    ARTIST("artist"),
    ALBUM("album"),
    ALBUM_ARTIST("albumartist"),
    TITLE("title"),
    TRACK("track"),
    GENRE("genre"),
    DATE("date"),
    COMPOSER("composer"),
    PERFORMER("performer"),
    DISC("disc"),
    LABEL("label"),
    AUDIO_FORMAT("AudioFormat"),
    FILE("file");

    private final String value;

    Tag(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }
}
