package com.gitlab.josercl.mpd.enums;

import java.util.Arrays;

public enum Subsystem {
    DATABASE("database"),
    UPDATE("update"),
    STORED_PLAYLIST("stored_playlist"),
    QUEUE("playlist"),
    PLAYER("player"),
    MIXER("mixer"),
    OUTPUT("output"),
    OPTIONS("options"),
    PARTITION("partition"),
    STICKER("sticker"),
    SUBSCRIPTION("subscription"),
    MESSAGE("message"),
    NEIGHBOR("neighbor"),
    MOUNT("mount"),
    INVALID("invalid");

    private final String value;

    Subsystem(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Subsystem fromValue(String val) {
        return Arrays.stream(values())
                .filter(ss -> ss.getValue().equals(val))
                .findFirst()
                .orElse(INVALID);
    }
}
