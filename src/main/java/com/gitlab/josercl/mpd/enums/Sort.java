package com.gitlab.josercl.mpd.enums;

public enum Sort {
    ARTIST("ArtistSort"),
    ALBUM("AlbumSort"),
    ALBUM_ARTIST("AlbumArtistSort");

    private final String value;

    Sort(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "sort " + this.getValue();
    }
}
