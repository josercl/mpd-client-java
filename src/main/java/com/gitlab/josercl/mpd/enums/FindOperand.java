package com.gitlab.josercl.mpd.enums;

public enum FindOperand {
    EQUALS("=="),
    NOT_EQUALS("!="),
    CONTAINS("contains"),
    LIKE("=~"),
    NOT_LIKE("!~"),
    AND("AND");

    private final String value;

    FindOperand(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }
}
