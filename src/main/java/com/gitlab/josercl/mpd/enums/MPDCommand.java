package com.gitlab.josercl.mpd.enums;

public enum MPDCommand {
    STATUS("status"),
    PLAY("play"),
    NEXT("next"),
    PREV("previous"),
    PAUSE("pause"),
    STOP("stop"),
    PLAY_SONG("playid"),
    RANDOM("random"),
    REPEAT("repeat"),
    CONSUME("consume"),
    SEEK("seekcur"),
    VOLUME("setvol"),
    CURRENT_SONG("currentsong"),
    PLAYLIST_INFO("playlistinfo"),
    PLAYLIST_FIND("playlistfind"),
    PLAYLIST_SEARCH("playlistsearch"),
    MOVE_SONG("moveid"),
    MOVE("move"),
    LIST("list"),
    LIST_ALL_INFO("listallinfo"),
    FIND("find"),
    SEARCH("search"),
    ADD("addid"),
    REMOVE("deleteid"),
    CLEAR("clear"),
    PRIORITY("prioid"),
    SINGLE("single"),
    ALBUM_ART("albumart"),
    OUTPUTS("outputs"),
    DISABLE_OUTPUT("disableoutput"),
    ENABLE_OUTPUT("enableoutput"),
    TOGGLE_OUTPUT("toggleoutput"),
    IDLE("idle"),
    UPDATE("update");

    private final String value;

    MPDCommand(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
