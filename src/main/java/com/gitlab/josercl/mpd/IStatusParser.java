package com.gitlab.josercl.mpd;

import com.gitlab.josercl.mpd.exception.InvalidFormatException;
import com.gitlab.josercl.mpd.model.Status;

import java.util.List;

public interface IStatusParser {
    String PARTITION = "partition";
    String VOLUME = "volume";
    String REPEAT = "repeat";
    String RANDOM = "random";
    String SINGLE = "single";
    String CONSUME = "consume";
    String PLAYLIST = "playlist";
    String PLAYLIST_LENGTH = "playlistlength";
    String STATE = "state";
    String SONG = "song";
    String SONG_ID = "songid";
    String NEXT_SONG = "nextsong";
    String NEXT_SONG_ID = "nextsongid";
    String ELAPSED = "elapsed";
    String DURATION = "duration";
    String BITRATE = "bitrate";
    String CROSSFADE = "xfade";
    String MIXRAMP = "mixrampdb";
    String MIXRAMP_DELAY = "mixrampdelay";
    String AUDIO = "audio";
    String UPDATING_DB = "updating_db";
    String ERROR = "error";

    Status parse(List<String> lines) throws InvalidFormatException;
}
