package com.gitlab.josercl.mpd;

import com.gitlab.josercl.mpd.exception.InvalidResponseException;

import java.util.List;

public interface IChannel {
    String getHost();

    int getPort();

    void executeCommands(Command... commands) throws InvalidResponseException;

    List<String> executeCommand(Command command) throws InvalidResponseException;
}
