package com.gitlab.josercl.mpd;

import com.gitlab.josercl.mpd.enums.MPDCommand;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class Command {
    private final MPDCommand command;
    private final String[] args;

    public Command(MPDCommand command, String... args) {
        this.command = command;
        this.args = args;
    }

    @Override
    public String toString() {
        return (command.getValue() + " " +
                Arrays.stream(args)
                        .filter(s -> Objects.nonNull(s) && !s.trim().isEmpty())
                        .collect(Collectors.joining(" "))).trim();
    }
}
