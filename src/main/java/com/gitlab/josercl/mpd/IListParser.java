package com.gitlab.josercl.mpd;

import com.gitlab.josercl.mpd.exception.InvalidFormatException;

import java.util.List;

public interface IListParser<T> {
    List<T> parse(List<String> lines) throws InvalidFormatException;
}
