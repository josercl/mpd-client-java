package com.gitlab.josercl.mpd.parser;

import com.gitlab.josercl.mpd.IListParser;

import java.util.List;

public class NoOpParser implements IListParser<String> {
    @Override
    public List<String> parse(List<String> lines) {
        return lines;
    }
}
