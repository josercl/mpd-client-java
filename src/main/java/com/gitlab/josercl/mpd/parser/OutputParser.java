package com.gitlab.josercl.mpd.parser;

import com.gitlab.josercl.mpd.IOutputParser;
import com.gitlab.josercl.mpd.exception.InvalidFormatException;
import com.gitlab.josercl.mpd.model.Output;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OutputParser implements IOutputParser {
    @Override
    public Output parse(List<String> lines) throws InvalidFormatException {
        Output.Builder builder = new Output.Builder();
        for (String line : lines) {
            if (!line.contains(": ")) {
                throw new InvalidFormatException("Invalid syntax, \":\" not found in line: " + line);
            }
            final String[] pieces = line.split(": ");

            updateOutput(builder, pieces[0].trim(), pieces[1].trim());
        }
        return builder.build();
    }

    @Override
    public List<Output> parseOutputList(List<String> response) throws InvalidFormatException {
        final List<List<String>> outputInfoList = new ArrayList<>();

        List<String> current = new ArrayList<>();
        boolean firstId = true;
        for (String line : response) {
            final String[] pieces = line.split(": ");

            if (!firstId && pieces[0].equals(IOutputParser.ID)) {
                outputInfoList.add(new ArrayList<>(current));
                current.clear();
            }

            if (firstId && pieces[0].equals(IOutputParser.ID)) {
                firstId = false;
            }

            current.add(line);
        }

        if (!current.isEmpty()) {
            outputInfoList.add(new ArrayList<>(current));
            current.clear();
        }

        return outputInfoList.stream()
                .map(this::parse)
                .collect(Collectors.toList());
    }

    private void updateOutput(Output.Builder builder, String key, String value) {
        switch (key) {
            case ID:
                builder.setId(Long.parseLong(value));
                break;
            case NAME:
                builder.setName(value);
                break;
            case PLUGIN:
                builder.setPlugin(value);
                break;
            case ENABLED:
                builder.setEnabled(value.equals("1"));
                break;
        }
    }
}
