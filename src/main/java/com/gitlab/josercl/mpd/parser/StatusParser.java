package com.gitlab.josercl.mpd.parser;

import com.gitlab.josercl.mpd.IStatusParser;
import com.gitlab.josercl.mpd.enums.Single;
import com.gitlab.josercl.mpd.enums.State;
import com.gitlab.josercl.mpd.exception.InvalidFormatException;
import com.gitlab.josercl.mpd.model.Status;

import java.util.List;

public class StatusParser implements IStatusParser {
    @Override
    public Status parse(List<String> lines) throws InvalidFormatException {
        final Status.Builder statusBuilder = new Status.Builder();
        for (String line : lines) {
            if (!line.contains(": ")) {
                throw new InvalidFormatException("Invalid syntax, \":\" not found in line: " + line);
            }
            final String[] pieces = line.split(": ");

            updateStatus(statusBuilder, pieces[0].trim(), pieces[1].trim());
        }
        return statusBuilder.build();
    }

    private static void updateStatus(Status.Builder builder, String key, String value) {
        switch (key) {
            case PARTITION:
                builder.setPartition(value);
                break;
            case VOLUME:
                builder.setVolume(Integer.parseInt(value));
                break;
            case REPEAT:
                builder.setRepeat(Integer.parseInt(value) == 1);
                break;
            case RANDOM:
                builder.setRandom(Integer.parseInt(value) == 1);
                break;
            case SINGLE:
                builder.setSingle(Single.fromValue(value));
                break;
            case CONSUME:
                builder.setConsume(Integer.parseInt(value) == 1);
                break;
            case PLAYLIST:
                builder.setPlaylist(Long.parseLong(value));
                break;
            case PLAYLIST_LENGTH:
                builder.setPlaylistLength(Integer.parseInt(value));
                break;
            case STATE:
                builder.setState(State.fromValue(value));
                break;
            case SONG:
                builder.setSong(Integer.parseInt(value));
                break;
            case SONG_ID:
                builder.setSongId(Long.parseLong(value));
                break;
            case NEXT_SONG:
                builder.setNextSong(Integer.parseInt(value));
                break;
            case NEXT_SONG_ID:
                builder.setNextSongId(Long.parseLong(value));
                break;
            case ELAPSED:
                builder.setElapsed(Double.parseDouble(value));
                break;
            case DURATION:
                builder.setDuration(Double.parseDouble(value));
                break;
            case BITRATE:
                builder.setBitrate(Integer.parseInt(value));
                break;
            case CROSSFADE:
                builder.setCrossfade(Double.parseDouble(value));
                break;
            case MIXRAMP:
                builder.setMixrampdb(Double.parseDouble(value));
                break;
            case MIXRAMP_DELAY:
                builder.setMixrampdelay(Double.parseDouble(value));
                break;
            case AUDIO:
                builder.setAudio(value);
                break;
            case UPDATING_DB:
                builder.setUpdatingDb(Long.parseLong(value));
                break;
            case ERROR:
                builder.setError(value);
                break;
        }
    }
}
