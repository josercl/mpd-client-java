package com.gitlab.josercl.mpd.parser;

import com.gitlab.josercl.mpd.ISongParser;
import com.gitlab.josercl.mpd.exception.InvalidFormatException;
import com.gitlab.josercl.mpd.model.Song;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SongParser implements ISongParser {
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    @Override
    public Song parse(List<String> lines) throws InvalidFormatException {
        Song.Builder songBuilder = new Song.Builder();
        for (String line : lines) {
            if (!line.contains(": ")) {
                throw new InvalidFormatException("Invalid syntax, \":\" not found in line: " + line);
            }
            final String[] pieces = line.split(": ", 2);

            updateSong(songBuilder, pieces[0].trim(), pieces[1].trim());
        }
        return songBuilder.build();
    }

    public List<Song> parseSongList(List<String> response) {
        final List<List<String>> songInfoList = new ArrayList<>();

        List<String> current = new ArrayList<>();
        boolean firstFile = true;
        for (String line : response) {
            final String[] pieces = line.split(": ");

            if (!firstFile && pieces[0].equals(ISongParser.FILE)) {
                songInfoList.add(new ArrayList<>(current));
                current.clear();
            }

            if (firstFile && pieces[0].equals(ISongParser.FILE)) {
                firstFile = false;
            }

            current.add(line);
        }

        if (!current.isEmpty()) {
            songInfoList.add(new ArrayList<>(current));
            current.clear();
        }

        return songInfoList.stream()
                .map(this::parse)
                .collect(Collectors.toList());
    }

    private void updateSong(Song.Builder builder, String key, String value) {
        switch (key) {
            case FILE:
                builder.setFile(value);
                break;
            case LAST_MODIFIED:
                try {
                    builder.setLastModified(simpleDateFormat.parse(value));
                } catch (ParseException ignored) {

                }
                break;
            case ARTIST:
                builder.setArtist(value);
                break;
            case ALBUM_ARTIST:
                builder.setAlbumArtist(value);
                break;
            case TITLE:
                builder.setTitle(value);
                break;
            case ALBUM:
                builder.setAlbum(value);
                break;
            case TRACK:
                builder.setTrack(Integer.parseInt(value));
                break;
            case DATE:
                builder.setDate(value);
                break;
            case GENRE:
                builder.setGenre(value);
                break;
            case CONDUCTOR:
                builder.setConductor(value);
                break;
            case GROUPING:
                builder.setGrouping(value);
                break;
            case LABEL:
                builder.setLabel(value);
                break;
            case DURATION:
                builder.setDuration(Integer.parseInt(value));
                break;
            case POSITION:
                builder.setPos(Integer.parseInt(value));
                break;
            case ID:
                builder.setId(Long.parseLong(value));
                break;
        }
    }
}
