package com.gitlab.josercl.mpd;

import com.gitlab.josercl.mpd.exception.InvalidFormatException;
import com.gitlab.josercl.mpd.model.Output;

import java.util.List;

public interface IOutputParser {
    String ID = "outputid";
    String NAME = "outputname";
    String PLUGIN = "plugin";
    String ENABLED = "outputenabled";

    Output parse(List<String> lines) throws InvalidFormatException;
    List<Output> parseOutputList(List<String> lines) throws InvalidFormatException;
}
