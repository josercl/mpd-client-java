package com.gitlab.josercl.mpd.condition;

import com.gitlab.josercl.mpd.ICondition;
import com.gitlab.josercl.mpd.util.Collectors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DisjunctionCondition implements ICondition {
    private final List<ICondition> conditions = new ArrayList<>();

    public DisjunctionCondition(ICondition condition1, ICondition... condition2) {
        conditions.add(condition1);
        conditions.addAll(Arrays.asList(condition2));
    }

    public DisjunctionCondition or(ICondition condition) {
        conditions.add(condition);
        return this;
    }

    @Override
    public String toString() {
        return "";
//        return conditions.stream()
//                .map(ICondition::toString)
//                .collect(Collectors.joining(" "));
    }

    @Override
    public String[] toStringArray() {
        return conditions.stream()
                .map(ICondition::toString)
                .collect(Collectors.toArray(String.class));
    }
}
