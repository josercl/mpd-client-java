package com.gitlab.josercl.mpd.condition;

import com.gitlab.josercl.mpd.ICondition;
import com.gitlab.josercl.mpd.enums.FindOperand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Conjunction (logical AND) of two or more conditions
 */
public class ConjunctionCondition implements ICondition {
    private final FindOperand operand = FindOperand.AND;
    private final List<ICondition> conditions = new ArrayList<>();

    public ConjunctionCondition(ICondition condition1, ICondition... condition2) {
        conditions.add(condition1);
        conditions.addAll(Arrays.asList(condition2));
    }

    public ConjunctionCondition and(ICondition condition) {
        conditions.add(condition);
        return this;
    }

    @Override
    public String toString() {
        return "(" +
                conditions.stream()
                        .map(ICondition::toString)
                        .collect(Collectors.joining(" " + operand + " ")) +
                ")";
    }

    @Override
    public String[] toStringArray() {
        return new String[]{toString()};
    }
}
