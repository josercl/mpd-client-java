package com.gitlab.josercl.mpd.condition;

import com.gitlab.josercl.mpd.ICondition;
import com.gitlab.josercl.mpd.enums.FindOperand;
import com.gitlab.josercl.mpd.enums.Tag;
import com.gitlab.josercl.mpd.util.EscapeUtil;

/**
 * Filter and Search conditions
 */
public class SearchCondition implements ICondition {
    private final FindOperand operand;
    private final Tag tag;
    private final String value;
    private boolean negated = false;

    private SearchCondition(FindOperand operand, Tag tag, String value) {
        this.operand = operand;
        this.tag = tag;
        this.value = value;
    }

    public static SearchCondition eq(Tag tag, String value) {
        return new SearchCondition(FindOperand.EQUALS, tag, value);
    }

    public static SearchCondition neq(Tag tag, String value) {
        return new SearchCondition(FindOperand.NOT_EQUALS, tag, value);
    }

    public static SearchCondition contains(Tag tag, String value) {
        return new SearchCondition(FindOperand.CONTAINS, tag, value);
    }

    public ConjunctionCondition and(ICondition ...condition) {
        return new ConjunctionCondition(this, condition);
    }

    public DisjunctionCondition or(ICondition ...condition) {
        return new DisjunctionCondition(this, condition);
    }

    public SearchCondition not() {
        this.negated = !negated;
        return this;
    }

    @Override
    public String toString() {
        String prefix = negated ? "(!" : "";
        String suffix = negated ? ")" : "";
        return prefix + "(" + tag + " " + operand + " " + EscapeUtil.quote(value) + ")" + suffix;
    }

    @Override
    public String[] toStringArray() {
        return new String[]{toString()};
    }
}
