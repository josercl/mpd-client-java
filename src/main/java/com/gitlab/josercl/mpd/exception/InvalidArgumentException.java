package com.gitlab.josercl.mpd.exception;

public class InvalidArgumentException extends Throwable {
    public InvalidArgumentException(String s) {
        super(s);
    }
}
