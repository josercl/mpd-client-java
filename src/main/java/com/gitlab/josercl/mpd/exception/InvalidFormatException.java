package com.gitlab.josercl.mpd.exception;

public class InvalidFormatException extends RuntimeException{
    public InvalidFormatException(String s) {
        super(s);
    }
}
