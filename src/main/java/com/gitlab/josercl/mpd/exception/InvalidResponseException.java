package com.gitlab.josercl.mpd.exception;

public class InvalidResponseException extends RuntimeException{
    public InvalidResponseException(String message) {
        super(message);
    }
}
