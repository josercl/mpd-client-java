package com.gitlab.josercl.mpd;

import com.gitlab.josercl.mpd.condition.ConjunctionCondition;
import com.gitlab.josercl.mpd.condition.SearchCondition;
import com.gitlab.josercl.mpd.enums.*;
import com.gitlab.josercl.mpd.exception.InvalidArgumentException;
import com.gitlab.josercl.mpd.model.Output;
import com.gitlab.josercl.mpd.model.Song;
import com.gitlab.josercl.mpd.model.Status;
import com.gitlab.josercl.mpd.parser.NoOpParser;
import com.gitlab.josercl.mpd.parser.OutputParser;
import com.gitlab.josercl.mpd.parser.SongParser;
import com.gitlab.josercl.mpd.parser.StatusParser;
import com.gitlab.josercl.mpd.util.Collectors;
import com.gitlab.josercl.mpd.util.EscapeUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class Client {
    private final IChannel channel;
    private final IStatusParser statusParser;
    private final ISongParser songParser;
    private final IOutputParser outputParser;

    public Client(IChannel channel, IStatusParser statusParser, ISongParser songParser, IOutputParser outputParser) {
        this.channel = channel;
        this.statusParser = statusParser;
        this.songParser = songParser;
        this.outputParser = outputParser;
    }

    public Client(String host, int port) {
        this.channel = new Channel(host, port);
        this.statusParser = new StatusParser();
        this.songParser = new SongParser();
        this.outputParser = new OutputParser();
    }

    public String getHost() {
        return channel.getHost();
    }

    public Integer getPort() {
        return channel.getPort();
    }

    public IStatusParser getStatusParser() {
        return statusParser;
    }

    public ISongParser getSongParser() {
        return songParser;
    }

    public IOutputParser getOutputParser() {
        return outputParser;
    }

    public void execute(Command... commands) {
        channel.executeCommands(commands);
    }

    /**
     * Reports the current status of the player and the volume level.
     * <ul>
     * <li>partition: the name of the current partition</li>
     * <li>volume: 0-100 (deprecated: -1 if the volume cannot be determined)</li>
     * <li>repeat: 0 or 1</li>
     * <li>random: 0 or 1</li>
     * <li>single: 0, 1, or oneshot</li>
     * <li>consume: 0 or 1</li>
     * <li>playlist: 31-bit unsigned integer, the playlist version number</li>
     * <li>playlistlength: integer, the length of the playlist</li>
     * <li>state: play, stop, or pause</li>
     * <li>song: playlist song number of the current song stopped on or playing</li>
     * <li>songid: playlist songid of the current song stopped on or playing</li>
     * <li>nextsong: playlist song number of the next song to be played</li>
     * <li>nextsongid: playlist songid of the next song to be played</li>
     * <li>elapsed: Total time elapsed within the current song in seconds, but with higher resolution.</li>
     * <li>duration: Duration of the current song in seconds.</li>
     * <li>bitrate: instantaneous bitrate in kbps</li>
     * <li>xfade: crossfade in seconds</li>
     * <li>mixrampdb: mixramp threshold in dB</li>
     * <li>mixrampdelay: mixrampdelay in seconds</li>
     * <li>audio: The format emitted by the decoder plugin during playback, format: samplerate:bits:channels.</li>
     * <li>updating_db: job id</li>
     * <li>error: if there is an error, returns message here</li>
     * </ul>
     *
     * @return {@link Status}
     */
    public Status getStatus() {
        return statusParser.parse(channel.executeCommand(new Command(MPDCommand.STATUS)));
    }

    /**
     * Clears the queue.
     */
    public void clear() {
        channel.executeCommand(new Command(MPDCommand.CLEAR));
    }

    /**
     * Updates mpd database
     */
    public void update() {
        channel.executeCommand(new Command(MPDCommand.UPDATE));
    }

    /**
     * Begins playing the playlist at song number SONGPOS.
     *
     * @param songPos Song position in the playlist, nullable
     */
    public void play(Integer songPos) {
        channel.executeCommand(
                new Command(
                        MPDCommand.PLAY,
                        Optional.ofNullable(songPos)
                                .map(Object::toString)
                                .orElse("")
                )
        );
    }

    public void play() {
        this.play(null);
    }

    /**
     * Begins playing the playlist at song SONGID.
     *
     * @param songId The song id in the playlist
     */
    public void playId(long songId) {
        channel.executeCommand(new Command(MPDCommand.PLAY_SONG, Long.toString(songId)));
    }

    /**
     * Deletes the song SONGID from the playlist
     *
     * @param songId The song id in the playlist
     */
    public void remove(long songId) {
        channel.executeCommand(new Command(MPDCommand.REMOVE, Long.toString(songId)));
    }

    /**
     * Adds a song to the playlist (non-recursive) and returns the song id. songPath is always a single file or URL
     *
     * @param songPath The path of the song in the library
     * @return The newly added song id
     */
    public Long add(String songPath) {
        final List<String> lines = channel.executeCommand(new Command(MPDCommand.ADD, EscapeUtil.quote(songPath)));
        return lines.stream()
                .map(line -> line.split(": "))
                .filter(pieces -> pieces[0].startsWith(ISongParser.ID))
                .findFirst()
                .map(s -> Long.parseLong(s[1]))
                .orElse(0L);
    }

    /**
     * pause {STATE}
     * Pause or resume playback.
     *
     * @param state True to pause playback or false to resume playback
     */
    public void pause(Boolean state) {
        channel.executeCommand(
                new Command(
                        MPDCommand.PAUSE,
                        Optional.ofNullable(state)
                                .map(b -> b ? "1" : "0")
                                .orElse("")
                )
        );
    }

    /**
     * pause {STATE}
     * Toggle pause state
     */
    public void pause() {
        this.pause(null);
    }

    /**
     * Stops playing.
     */
    public void stop() {
        channel.executeCommand(new Command(MPDCommand.STOP));
    }

    /**
     * Plays previous song in the playlist.
     */
    public void previous() {
        channel.executeCommand(new Command(MPDCommand.PREV));
    }

    /**
     * Plays next song in the playlist.
     */
    public void next() {
        channel.executeCommand(new Command(MPDCommand.NEXT));
    }

    /**
     * Set queue random state.
     *
     * @param random true to enable, false to disable queue randomization
     */
    public void random(boolean random) {
        channel.executeCommand(new Command(MPDCommand.RANDOM, random ? "1" : "0"));
    }

    /**
     * Set queue repeat state.
     *
     * @param repeat true to enable, false to disable queue repeat
     */
    public void repeat(boolean repeat) {
        channel.executeCommand(new Command(MPDCommand.REPEAT, repeat ? "1" : "0"));
    }

    /**
     * Set queue consume state. When consume is activated, each song played is removed from playlist.
     *
     * @param consume true to enable, false to disable queue consuming
     */
    public void consume(boolean consume) {
        channel.executeCommand(new Command(MPDCommand.CONSUME, consume ? "1" : "0"));
    }

    /**
     * Sets volume, the range of volume is 0-100.
     *
     * @param volume The volume level
     */
    public void setVolume(int volume) {
        channel.executeCommand(new Command(MPDCommand.VOLUME, Integer.toString(volume)));
    }

    /**
     * Sets single state to STATE, STATE should be 0, 1 or oneshot 6. When single is activated, playback is stopped after current song, or song is repeated if the 'repeat' mode is enabled
     *
     * @param single Single state to set
     * @see #repeat(boolean)
     */
    public void setSingle(Single single) {
        if (single != Single.INVALID) {
            channel.executeCommand(new Command(MPDCommand.SINGLE, single.toString()));
        }
    }

    /**
     * Seeks to the position TIME (in seconds) within the current song
     *
     * @param time Time in seconds within the current song
     * @see #goForwards(int)
     * @see #goBackwards(int)
     */
    public void seekTo(int time) {
        channel.executeCommand(new Command(MPDCommand.SEEK, Integer.toString(time)));
    }

    /**
     * Skips forward from the current playing position
     *
     * @param time Time in seconds to skip forward
     */
    public void goForwards(int time) {
        channel.executeCommand(new Command(MPDCommand.SEEK, "+" + time));
    }

    /**
     * Rewind time in seconds from the current playing position
     *
     * @param time Time in seconds to skip forward
     */
    public void goBackwards(int time) {
        channel.executeCommand(new Command(MPDCommand.SEEK, "-" + time));
    }

    /**
     * Moves the song at 'from position' to 'to position' in the playlist.
     *
     * @param from original song position
     * @param to   final position
     */
    public void move(int from, int to) {
        channel.executeCommand(new Command(MPDCommand.MOVE, Integer.toString(from), Integer.toString(to)));
    }

    /**
     * Moves the song with songid to 'to position' (playlist index) in the playlist.
     *
     * @param songId The song id
     * @param to     the final position, if negative, it is relative to the current song in the playlist (if there is one)
     */
    public void moveSong(Long songId, int to) {
        channel.executeCommand(new Command(MPDCommand.MOVE_SONG, songId.toString(), Integer.toString(to)));
    }

    /**
     * Set the priority of the specified songs. A higher priority means that it will be played first when "random" mode is enabled.
     * A priority is an integer between 0 and 255. The default priority of new songs is 0.
     *
     * @param songId   The song id
     * @param priority priority, must be between 0-255
     * @throws InvalidArgumentException If the priority is outside the range 0 - 255
     */
    public void setPriority(Long songId, int priority) throws InvalidArgumentException {
        if (priority < 0 || priority > 255) {
            throw new InvalidArgumentException("Invalid priority: " + priority);
        }
        channel.executeCommand(new Command(MPDCommand.PRIORITY, Integer.toString(priority), songId.toString()));
    }

    /**
     * Displays the info of the current {@link Song} (same song that is identified in status)
     *
     * @return {@link Song}
     */
    public Song getCurrentSong() {
        return songParser.parse(channel.executeCommand(new Command(MPDCommand.CURRENT_SONG)));
    }

    /**
     * Get a list of all songs in the playlist
     *
     * @return List of {@link Song} in the queue
     */
    public List<Song> getQueue() {
        return songParser.parseSongList(channel.executeCommand(new Command(MPDCommand.PLAYLIST_INFO)));
    }

    private List<Song> findImpl(ICondition condition, String sort, boolean ignoreCase) {
        String[] args = condition.toStringArray();
        String[] argsArr = Arrays.stream(Arrays.copyOf(args, args.length + 1))
                .map(EscapeUtil::quote)
                .collect(Collectors.toArray(String.class));

        Optional.ofNullable(sort)
                .ifPresent(s -> argsArr[argsArr.length - 1] = s);

        return songParser.parseSongList(
                channel.executeCommand(
                        new Command(
                                ignoreCase ? MPDCommand.SEARCH : MPDCommand.FIND,
                                argsArr
                        )
                )
        );
    }

    public List<Song> find(ICondition condition, Tag sortTag, boolean ignoreCase) {
        return findImpl(
                condition,
                Optional.ofNullable(sortTag).map(s -> "sort " + sortTag).orElse(null),
                ignoreCase
        );
    }

    /**
     * Search the database for songs matching the {@link ICondition}
     *
     * @param condition  See {@link ICondition}
     * @param sort       Sort condition, if null then sort order is undefined
     * @param ignoreCase if true, the search ignores letter casing
     * @return List of {@link Song} that match the condition
     */
    public List<Song> find(ICondition condition, Sort sort, boolean ignoreCase) {
        return findImpl(
                condition,
                Optional.ofNullable(sort).map(Sort::toString).orElse(null),
                ignoreCase
        );
    }

    /**
     * Same as {@link #find(ICondition, Sort, boolean)} without sorting order
     *
     * @see #find(ICondition, Sort, boolean)
     */
    public List<Song> find(ICondition condition, boolean ignoreCase) {
        return this.find(condition, (Sort) null, ignoreCase);
    }

    /**
     * Same as {@link #find(ICondition, Sort, boolean)} without sorting order and ignoring case
     *
     * @see #find(ICondition, Sort, boolean)
     */
    public List<Song> find(ICondition condition) {
        return this.find(condition, (Sort) null, true);
    }

    /**
     * Finds songs in the queue.
     *
     * @param condition  See {@link ICondition}
     * @param ignoreCase if true, perform the search ignoring letter casing
     */
    public List<Song> queueFind(ICondition condition, boolean ignoreCase) {
        return songParser.parseSongList(
                channel.executeCommand(
                        new Command(
                                ignoreCase ? MPDCommand.PLAYLIST_SEARCH : MPDCommand.PLAYLIST_FIND,
                                EscapeUtil.quote(condition.toString())
                        )
                )
        );
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> list(Tag listTag, ICondition filter, Tag groupTag, IListParser<T> parser) {
        final Command command = new Command(
                MPDCommand.LIST,
                listTag.toString(),
                Optional.ofNullable(filter)
                        .map(c -> EscapeUtil.quote(c.toString()))
                        .orElse(""),
                Optional.ofNullable(groupTag)
                        .map(gt -> "group " + EscapeUtil.quote(gt.toString()))
                        .orElse("")
        );
        final List<String> response = channel.executeCommand(command);
        return Optional.ofNullable(parser)
                .orElse((IListParser<T>) new NoOpParser())
                .parse(response);
    }

    public List<Song> listAll() {
        return songParser.parseSongList(channel.executeCommand(new Command(MPDCommand.LIST_ALL_INFO)));
    }

    public List<Output> getOutputs() {
        return outputParser.parseOutputList(
                channel.executeCommand(new Command(MPDCommand.OUTPUTS))
        );
    }

    /**
     * Turns an output on or off, depending on the current state.
     *
     * @param outputId The output id to toggle
     */
    public void toggleOutput(Long outputId) {
        channel.executeCommand(new Command(MPDCommand.TOGGLE_OUTPUT, outputId.toString()));
    }

    public void disableOutput(Long outputId) {
        channel.executeCommand(new Command(MPDCommand.DISABLE_OUTPUT, outputId.toString()));
    }

    public void enableOutput(Long outputId) {
        channel.executeCommand(new Command(MPDCommand.ENABLE_OUTPUT, outputId.toString()));
    }

    public void enqueueAlbum(String artist, String album) {
        channel.executeCommands(getCommandsForAlbumSongs(artist, album, false));
    }

    public void playAlbum(String artist, String album) {
        channel.executeCommands(getCommandsForAlbumSongs(artist, album, true));
    }

    public void removeAlbumFromQueue(String artist, String album) {
        ConjunctionCondition searchCondition =
                SearchCondition.eq(Tag.ARTIST, artist).and(SearchCondition.eq(Tag.ALBUM, album));
        Command[] commands = queueFind(searchCondition, true)
                .stream()
                .map(song -> new Command(MPDCommand.REMOVE, Long.toString(song.getId())))
                .collect(Collectors.toArray(Command.class));
        channel.executeCommands(commands);
    }

    public void playNext(Long songId) {
        Command[] commands = getQueue()
                .stream()
                .map(song -> {
                    int newPriority = song.getId().equals(songId) ? 255 : 0;
                    return new Command(MPDCommand.PRIORITY, Integer.toString(newPriority), Long.toString(song.getId()));
                })
                .collect(Collectors.toArray(Command.class));
        channel.executeCommands(commands);
    }

    public Subsystem idle() {
        return channel.executeCommand(new Command(MPDCommand.IDLE))
                .stream()
                .filter(line -> line.startsWith("changed: "))
                .findFirst()
                .map(line -> Subsystem.fromValue(line.split(": ")[1]))
                .orElse(Subsystem.INVALID);
    }

    private Command[] getCommandsForAlbumSongs(
            String artist,
            String album,
            boolean play
    ) {
        ConjunctionCondition searchCondition =
                SearchCondition.eq(Tag.ARTIST, artist).and(SearchCondition.eq(Tag.ALBUM, album));

        Stream<Command> stream = find(searchCondition, true)
                .stream()
                .map(Song::getFile)
                .map(s -> new Command(MPDCommand.ADD, EscapeUtil.quote(s)));

        if (play) {
            Stream<Command> before = Stream.concat(Stream.of(new Command(MPDCommand.CLEAR)), stream);
            stream = Stream.concat(before, Stream.of(new Command(MPDCommand.PLAY)));
        }

        return stream.collect(Collectors.toArray(Command.class));
    }
}
