package com.gitlab.josercl.mpd;

import com.gitlab.josercl.mpd.exception.InvalidFormatException;
import com.gitlab.josercl.mpd.model.Song;

import java.util.List;

public interface ISongParser {
    String FILE = "file";
    String LAST_MODIFIED = "Last-Modified";
    String ARTIST = "Artist";
    String ALBUM_ARTIST = "AlbumArtist";
    String TITLE = "Title";
    String ALBUM = "Album";
    String TRACK = "Track";
    String DATE = "Date";
    String GENRE = "Genre";
    String CONDUCTOR = "Conductor";
    String GROUPING = "Grouping";
    String LABEL = "Label";
    String DURATION = "Time";
    String POSITION = "Pos";
    String ID = "Id";

    Song parse(List<String> lines) throws InvalidFormatException;
    List<Song> parseSongList(List<String> lines) throws InvalidFormatException;
}
