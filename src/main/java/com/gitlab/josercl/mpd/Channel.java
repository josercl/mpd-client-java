package com.gitlab.josercl.mpd;

import com.gitlab.josercl.mpd.exception.InvalidResponseException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Channel implements IChannel {
    private final static String MULTILINE_COMMAND_START = "command_list_begin\n";
    private final static String MULTILINE_COMMAND_END = "\ncommand_list_end";

    private final String host;
    private final int port;

    public Channel(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public String getHost() {
        return host;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public void executeCommands(Command... commands) throws InvalidResponseException {
        if (commands.length > 0) {
            final String cmd = MULTILINE_COMMAND_START +
                    Arrays.stream(commands).map(Command::toString).collect(Collectors.joining("\n")) +
                    MULTILINE_COMMAND_END;
            writeCommand(cmd);
        }
    }

    @Override
    public List<String> executeCommand(Command command) throws InvalidResponseException {
        return writeCommand(command.toString());
    }

    private List<String> writeCommand(String cmd) throws InvalidResponseException {
        List<String> result = new ArrayList<>();
        try (
                Socket socket = new Socket(this.host, this.port);
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                DataOutputStream out = new DataOutputStream(socket.getOutputStream())
        ) {
            br.readLine();

            out.write(cmd.getBytes(StandardCharsets.UTF_8));
            out.write('\n');

            String line;
            do {
                line = br.readLine();
                result.add(line);
            } while (line != null && !line.startsWith("ACK") && !line.equals("OK"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (result.isEmpty()) {
            throw new InvalidResponseException("Error executing command: " + cmd);
        }

        result.stream()
                .filter(l -> l.startsWith("ACK"))
                .findFirst()
                .ifPresent(l -> {
                    throw new InvalidResponseException("Error executing command: " + cmd + " - " + l);
                });

        result.remove(result.size() - 1);
        return result;
    }
}
